//
//  ViewController.m
//  FoodDelivery
//
//  Created by STUDENT on 9/25/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "LoginViewController.h"
#import "SelectFoodTableViewController.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorUsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorPasswordLabel;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)clearWarningMessage {
    self.errorUsernameLabel.textColor = [UIColor clearColor];
    self.errorPasswordLabel.textColor = [UIColor clearColor];
}

- (void)resetTextField {
    self.usernameTextField.text = @"";
    self.passwordTextField.text = @"";
}

- (void)setInitializeState {
    [self clearWarningMessage];
    [self resetTextField];
}

- (void)viewWillAppear:(BOOL)animated {
    [self setInitializeState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (bool)isUsernameValidate {
    return ![self.usernameTextField.text isEqualToString:@""];
}

- (bool)isPasswordValidate {
    return ![self.passwordTextField.text isEqualToString:@""];
}

- (void)checkLogin {
    bool isUsernameValidate = [self isUsernameValidate];
    bool isPasswordValidate = [self isPasswordValidate];
    if (isUsernameValidate && isPasswordValidate) {
        // Go to nextpage
        NSLog(@"Go To nextpage");
        [self performSegueWithIdentifier:@"goSelectFood" sender:self];
    } else {
        self.errorUsernameLabel.textColor = [UIColor clearColor];
        self.errorPasswordLabel.textColor = [UIColor clearColor];
        
        if (!isUsernameValidate) {
            // Warning message
            NSLog(@"no username given");
            self.errorUsernameLabel.textColor = [[UIColor alloc] initWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
        }
        if (!isPasswordValidate) {
            // Warning message
            NSLog(@"no password given");
            self.errorPasswordLabel.textColor = [[UIColor alloc] initWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
        }
    }
}

- (IBAction)loginButtonTapped:(id)sender {
    [self checkLogin];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqual:@"goSelectFood"]) {
        SelectFoodTableViewController *selectFoodViewController = [segue destinationViewController];
        selectFoodViewController.username = self.usernameTextField.text;
    }
}


@end
