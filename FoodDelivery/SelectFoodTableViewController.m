//
//  SelectFoodTableViewController.m
//  FoodDelivery
//
//  Created by STUDENT on 9/25/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "SelectFoodTableViewController.h"
#import "DeliveryTimeViewController.h"
#import "FoodList.h"
#import "FoodMenu.h"

@interface SelectFoodTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;

@end

@implementation SelectFoodTableViewController {
    FoodMenu *foodMenu;
    NSMutableArray *selectedFood;
}

- (void)loadData {
    FoodList *mainDishesFoodList = [[FoodList alloc] initWithArray:@[@"Pork steak", @"Beef steak", @"Salmon steak", @"Chicken"]
                                                       setFoodType:@"Main Dishes"];
    FoodList *dessertsFoodList = [[FoodList alloc] initWithArray:@[@"Ice cream", @"Fruit Salad"]
                                                     setFoodType:@"Desserts"];
    FoodList *drinksFoodList = [[FoodList alloc] initWithArray:@[@"Coca-Cola", @"Pepsi", @"Coffee"]
                                                   setFoodType:@"Drinks"];
    
    foodMenu = [[FoodMenu alloc] initWithArray:@[mainDishesFoodList, dessertsFoodList, drinksFoodList]];
    
    selectedFood = [[NSMutableArray alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"Welcome %@", self.username);
    self.welcomeLabel.text = [NSString stringWithFormat:@"   Welcome back! %@", self.username];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (bool)isSelectFoodComplete {
    
    if ([selectedFood count] < [foodMenu countFoodList]) {
        return NO;
    }
    return YES;
}

- (void)showAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected Incomplete"
                                                    message:@"Please select all food type."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStyleDefault;
    [alert show];
}

- (IBAction)nextButtonTapped:(id)sender {
    if ([self isSelectFoodComplete]) {
        [self performSegueWithIdentifier:@"goDeliveryTime" sender:self];
    } else {
        [self showAlert];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [foodMenu countFoodList];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [foodMenu countByFoodTypeNumber:section];
}

- (void)setCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    cell.textLabel.text = [foodMenu getFoodNameByTypeNumber:indexPath.section atIndex:indexPath.row];
    if ([selectedFood containsObject:indexPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"food" forIndexPath:indexPath];
    
    [self setCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [foodMenu getFoodTypeByTypeNumber:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *iteratorIndexPath;
    NSUInteger index = 0;
    while (index < [selectedFood count]) {
        iteratorIndexPath = selectedFood[index];
        if (iteratorIndexPath.section == indexPath.section) {
            [selectedFood removeObject:iteratorIndexPath];
            index -= 1;
        }
        index += 1;
    }
    [selectedFood addObject:indexPath];
    
    [tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqual:@"goDeliveryTime"]) {
        DeliveryTimeViewController *deliveyTimeViewController = [segue destinationViewController];
        NSMutableArray *selectedFoodName = [[NSMutableArray alloc] init];
        for (NSIndexPath *indexPath in selectedFood) {
            [selectedFoodName addObject:[foodMenu getFoodNameByTypeNumber:indexPath.section atIndex:indexPath.row]];
            NSLog(@"%@", [foodMenu getFoodNameByTypeNumber:indexPath.section atIndex:indexPath.row]);
        }
        deliveyTimeViewController.selectedFoodName = [NSArray arrayWithArray:selectedFoodName];
    }
}


@end
