//
//  FoodMenu.m
//  FoodDelivery
//
//  Created by STUDENT on 9/25/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "FoodMenu.h"
#import "foodList.h"

@implementation FoodMenu {
    NSDictionary *foodMenuDictionary;
    NSArray *foodTypeArray;
}

- (void)loadData:(NSArray *)foodListArray {
    NSMutableArray *tempFoodType = [[NSMutableArray alloc] init];
    NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
    for (FoodList *foodList in foodListArray) {
        [temp setObject:foodList forKey:foodList.foodType];
        [tempFoodType addObject:foodList.foodType];
    }
    foodMenuDictionary = [NSDictionary dictionaryWithDictionary:temp];
    foodTypeArray = [NSArray arrayWithArray:tempFoodType];
}

- (id)initWithArray:(NSArray *)foodListArray {
    self = [super init];
    if (self) {
        [self loadData:foodListArray];
    }
    return self;
}

- (NSString *)getFoodNameByType:(NSString *)foodType atIndex:(NSUInteger)index {
    return [[foodMenuDictionary objectForKey:foodType] getFoodNameAtIndex:index];
}

- (NSString *)getFoodNameByTypeNumber:(NSUInteger)foodTypeNumber atIndex:(NSUInteger)index {
    return [self getFoodNameByType:foodTypeArray[foodTypeNumber] atIndex:index];
}

- (NSString *)getFoodTypeByTypeNumber:(NSUInteger)foodTypeNumber {
    return foodTypeArray[foodTypeNumber];
}

- (NSUInteger)countFoodList {
    return [foodMenuDictionary count];
}

- (NSUInteger)countByFoodType:(NSString *)foodType {
    return [[foodMenuDictionary objectForKey:foodType] count];
}

- (NSUInteger)countByFoodTypeNumber:(NSUInteger)foodTypeNumber {
    return [self countByFoodType:foodTypeArray[foodTypeNumber]];
}

@end
