//
//  FoodList.h
//  FoodDelivery
//
//  Created by STUDENT on 9/25/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodList : NSObject

@property (strong, nonatomic) NSString *foodType;

- (id)initWithArray:(NSArray *)foodArray setFoodType:(NSString *)foodType;
- (NSString *)getFoodNameAtIndex:(NSUInteger)index;
- (NSUInteger)count;

@end
